#include <iostream>
#include <string>
#include <cctype>

// Проверка, является ли подстрока валидным номером карты с помощью алгоритма Луна
bool isValidCardNumber(const std::string& cardNumber) {
    int nDigits = cardNumber.length();
    int sum = 0;
    bool isSecond = false;

    for (int i = nDigits - 1; i >= 0; i--) {
        char ch = cardNumber[i];
        if (!isdigit(ch)) {
            return false;  // В номере карты есть недопустимый символ
        }

        int digit = ch - '0';

        if (isSecond) {
            digit = digit * 2;
            if (digit > 9) {
                digit -= 9;
            }
        }

        sum += digit;
        isSecond = !isSecond;
    }

    return (sum % 10 == 0);
}

// Поиск и вывод всех возможных номеров карт в длинной строке за один проход
void findAndPrintCardNumbers(const std::string& longString) {
    const int minLength = 13;
    const int maxLength = 19;
    bool found = false;

    for (size_t i = 0; i < longString.length(); ++i) {
        for (size_t length = minLength; length <= maxLength; ++length) {
            if (i + length <= longString.length()) {
                std::string subStr = longString.substr(i, length);
                if (isValidCardNumber(subStr)) {
                    std::cout << "Найден валидный номер карты: " << subStr << std::endl;
                    found = true;
                }
            }
        }
    }

    if (!found) {
        std::cout << "Валидные номера карт не найдены." << std::endl;
    }
}

int main() {
    std::string longString;
    std::cout << "Введите длинную строку из чисел: ";
    std::cin >> longString;

    findAndPrintCardNumbers(longString);

    return 0;
}
